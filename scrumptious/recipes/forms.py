from recipes.models import Recipe
from django.forms import ModelForm

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = (
            "name",
            "picture",
            "description",
        )
