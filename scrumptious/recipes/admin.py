from django.contrib import admin
from recipes.models import Recipe, RecipeStep,Ingredient
# Register your models here.
# admin.site.register(Recipe)

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
        "description",
    )

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "step_number",
        "id",
        "instruction",
    )

@admin.register(Ingredient)
class RecipeIngredientAdmin(admin.ModelAdmin):
    list_display = (
        "amount",
        "food_item",
        "id",
    )
